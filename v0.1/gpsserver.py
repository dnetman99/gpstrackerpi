#!/usr/bin/python -tt
#gpsserver.py
import ConfigParser
import urllib2, os
from serialGetGPS import startSerialCapture
import sys, time
from daemon import Daemon
from gpsLogger import gpslog

class MyDaemon(Daemon):
	def run(self):
		istatus = False
		config = ConfigParser.RawConfigParser()
		config.read('/etc/gpstrack.conf')
		gpsunit = config.get("parms", "unitnumber")
		while istatus == False:
			try:
				response=urllib2.urlopen('http://74.125.113.99',timeout=1)
				istatus = True
				##print 'Internet up starting serial Capture'
				gpslog('INFO', 'Internet up starting serial Capture')
				startSerialCapture(gpsunit)
			except urllib2.URLError as err:
				pass
				istatus = False
				print 'No internet still testing before serial capture starts'
				gpslog('ERROR', 'No internet still testing before serial capture starts')
				time.sleep(1)

if __name__ == "__main__":
	daemon = MyDaemon('/var/run/gpsserver.pid')
	if len(sys.argv) == 2:
		if 'start' == sys.argv[1]:
			gpslog('INFO', 'GPS Server Started')
			daemon.start()
		elif 'stop' == sys.argv[1]:
			gpslog('INFO', 'GPS Server Stopped')
			daemon.stop()
		elif 'restart' == sys.argv[1]:
			gpslog('INFO', 'GPS Server Restarting')
			daemon.restart()
		else:
			print "Unknown command"
			sys.exit(2)
		sys.exit(0)
	else:
		print "usage: %s start|stop|restart" % sys.argv[0]
		sys.exit(2)