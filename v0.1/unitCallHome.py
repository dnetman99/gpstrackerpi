#!/usr/bin/python -tt

import urllib
import urllib2
import wddx, time
from gpsLogger import gpslog
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('/etc/gpstrack.conf')
url = config.get("parms", "url")

def callhome(trackunit):
	try:
		params = urllib.urlencode({
		'method': 'unitCallHome',
		'unit': trackunit
		})
		response = urllib2.urlopen(url, params).read()
		test = wddx.loads(response)
		gpslog('INFO', 'CALL HOME EXECUTED'+ str(test[0]))
		
	except urllib2.URLError as err:
		gpslog('ERROR', 'CALL HOME ERROR: ' + err)
		pass
	